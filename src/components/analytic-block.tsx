import React from 'react';

const AnalyticBlock = () => {
  // code

  return (
    <div className="flex flex-col md:flex-row md:justify-between mt-2 mb-4 h-auto">
      <div className="w-full mb-2 md:mr-2 md:w-1/2 px-4 py-2 border border-black">
        <p>Block with graphic</p>
        <p>Block with graphic</p>
        <p>Block with graphic</p>
        <p>Block with graphic</p>
        <p>Block with graphic</p>
        <p>Block with graphic</p>
        <p>Block with graphic</p>
        <p>Block with graphic</p>
      </div>
      <div className="w-full mb-2 md:ml-2 md:w-1/2 px-4 py-2 border border-black">
        <p>Block with analytic</p>
        <p>Block with analytic</p>
        <p>Block with analytic</p>
        <p>Block with analytic</p>
        <p>Block with analytic</p>
        <p>Block with analytic</p>
        <p>Block with analytic</p>
        <p>Block with analytic</p>
      </div>
    </div>
  );
};

export default React.memo(AnalyticBlock);
