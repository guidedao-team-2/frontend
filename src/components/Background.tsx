import React, { useState, useEffect } from 'react';

const Background = () => {
  const [colors, setColors] = useState({
    color1: 'rgba(96,120,249,1)',
    color2: 'rgba(246,247,254,1)'
  });

  useEffect(() => {
    const updateColors = () => {
      // Calculate new colors based on sine wave
      const currentTime = new Date().getTime();
      const sinValue = Math.sin(currentTime / 1000); // You can adjust the speed of color change here
      const newColor1 = `rgba(${Math.floor(96 + 40 * sinValue)}, ${Math.floor(
        120 + 40 * sinValue
      )}, 249, 1)`;
      const newColor2 = `rgba(${Math.floor(246 + 8 * sinValue)}, ${Math.floor(
        247 + 8 * sinValue
      )}, 254, 1)`;

      setColors({
        color1: newColor1,
        color2: newColor2
      });
    };

    const intervalId = setInterval(updateColors, 10); // You can adjust the interval here

    return () => clearInterval(intervalId);
  }, []);

  return (
    <div className="fixed top-0 left-0 w-full h-full z-[-1]">
      <div
        className="absolute top-0 left-0 w-full h-full"
        style={{
          background: `linear-gradient(to bottom, ${colors.color1}, ${colors.color2})`
        }}
      />
    </div>
  );
};

export default React.memo(Background);
