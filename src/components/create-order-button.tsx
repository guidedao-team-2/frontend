import { DropdownContext } from '@/contexts/dropdown-context';
import { PurchaseContext } from '@/contexts/purchase-context';
import React, { useContext, useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';

interface CurrencyOption {
  label: string;
  value: string;
  image: string;
}

const CreateOrderButton = () => {
  const { buy, setBuy, sell, setSell, coinAmount, setCoinAmount } =
    useContext(PurchaseContext);
  const { selectedOption, setSelectedOption } = useContext(DropdownContext);
  const [createButtonOn, setCreateButtonOn] = useState(false);
  const [orders, setOrders] = useState<
    {
      id: string;
      selectedOption: CurrencyOption | undefined;
      type: string;
      coinAmount: any;
    }[]
  >([]);
  //Get localstogare data
  useEffect(() => {
    const storedOrders = JSON.parse(localStorage.getItem('orders') ?? '[]');
    setOrders(storedOrders);
  }, []);

  //захардкожены переменные для проверки:
  const fee = { currency: 'USDC', value: 120 };
  //конец хардкод блока

  // UseEffect for set ui-border for user
  useEffect(() => {
    if (
      selectedOption &&
      selectedOption.value !== '' &&
      coinAmount !== '' &&
      (buy || sell)
    ) {
      setCreateButtonOn(true);
    } else {
      setCreateButtonOn(false);
    }
  }, [coinAmount, buy, sell, selectedOption]);

  function createOrder() {
    if (
      selectedOption &&
      selectedOption.value !== '' &&
      coinAmount !== '' &&
      (buy || sell)
    ) {
      let type = '';
      if (buy) {
        type = 'BUY';
      } else if (sell) {
        type = 'SELL';
      }
      const addOrder = { id: uuidv4(), selectedOption, type, coinAmount, fee };
      setOrders((prevOrders) => [...prevOrders, addOrder]);
    }
  }

  useEffect(() => {
    localStorage.setItem('orders', JSON.stringify(orders));
    console.log(orders);
  }, [orders]);

  return (
    <div className="flex justify-between mt-6 mb-8">
      <button
        onClick={createOrder}
        className={`w-full px-4 py-2 ${
          createButtonOn ? 'border-4  border-purple-900' : 'border border-black'
        }  bg-blue-500 text-white rounded-md`}
      >
        Create Order
      </button>
    </div>
  );
};

export default React.memo(CreateOrderButton);
