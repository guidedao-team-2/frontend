import React from 'react';
import { useTheme } from 'next-themes';
import { useState, useEffect } from 'react';

const ThemeButton = () => {
  const { theme, setTheme } = useTheme();
  const [mounted, setMounted] = useState(false);

  function handleThemeChange() {
    if (theme === 'light') {
      setTheme('dark');
    } else {
      setTheme('light');
    }
  }

  useEffect(() => {
    setMounted(true);
  }, []);

  if (!mounted) {
    return null;
  }

  return (
    <div>
      <button
        className="bg-gray-300 dark:bg-gray-700 text-gray-700 dark:text-gray-300 px-4 py-2 rounded"
        onClick={handleThemeChange}
      >
        {theme}
      </button>
    </div>
  );
};

export default React.memo(ThemeButton);
