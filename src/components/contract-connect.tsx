import { ethers } from 'ethers';
import { useEffect, useState } from 'react';

const contractAddress = '0x53a04049fd790f5A356c634C4c99f787D4Da764D'; //Address of our contract
const contractABI = [
  'function nonfungiblePositionManager() public view returns (address)'
];

function ContractConnect() {
  const [positionManager, setPositionManager] = useState('');

  useEffect(() => {
    const provider = new ethers.JsonRpcProvider('https://polygon-rpc.com/');
    const contract = new ethers.Contract(
      contractAddress,
      contractABI,
      provider
    );

    async function fetchNonfungiblePositionManager() {
      const nonfungiblePositionManager =
        await contract.nonfungiblePositionManager();
      setPositionManager(nonfungiblePositionManager);
    }

    fetchNonfungiblePositionManager();
  }, []);

  return (
    <div>
      <p>Nonfungible Position Manager: {positionManager}</p>
    </div>
  );
}

export default ContractConnect;
