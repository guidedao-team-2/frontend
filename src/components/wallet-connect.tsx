'use client';

/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState, useContext } from 'react';
import { BrowserProvider, InfuraProvider, JsonRpcProvider } from 'ethers';
import { useRouter } from 'next/navigation';
import { AuthContext } from '../contexts/loging';

interface WalletConnectProps {}

const WalletConnect: React.FunctionComponent<WalletConnectProps> = () => {
  const { isLogged, setIsLogged }: any = useContext(AuthContext);
  const router = useRouter();
  const [currentAccount, setCurrentAccount] = useState<bigint | any>();
  const [etherBalance, setEtherBalance] = useState<bigint | any>();
  const provider: InfuraProvider | JsonRpcProvider = new InfuraProvider(
    'goerli'
  );

  const getBalance = async () => {
    const balance: bigint = await provider.getBalance(currentAccount);
    setEtherBalance(balance);
    return balance;
  };

  useEffect(() => {
    getBalance().then(setEtherBalance).catch(console.error);
  }, [currentAccount]);

  const handleConnectClick = async () => {
    const provider = new BrowserProvider(window.ethereum);
    const accounts = await provider.send('eth_requestAccounts', []);
    setCurrentAccount(accounts[0]);

    setIsLogged(true);
    router.push('/Order');
  };

  return (
    <div className=" fixed border border-black bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 right-4 top-4 rounded focus:outline-none focus:shadow-outline">
      <button onClick={handleConnectClick}>Connect W3</button>
    </div>
  );
};

export default React.memo(WalletConnect);
