import { PurchaseContext } from '@/contexts/purchase-context';
import React, { useContext } from 'react';

const ButtonBlock = () => {
  const { buy, setBuy, sell, setSell } = useContext(PurchaseContext);

  function handleBuy() {
    setBuy(!buy);
    setSell(false);
  }

  function handleSell() {
    setSell(!sell);
    setBuy(false);
  }
  return (
    <div className="flex justify-between mt-6 mb-2">
      <button
        onClick={handleBuy}
        className={`mr-2 w-1/2 px-4 py-2 ${
          buy ? 'border-4 py-3 ' : 'border'
        } border-black ${
          sell ? 'from-gray-700' : 'bg-blue-500'
        }  text-white rounded-md`}
      >
        Buy
      </button>
      <button
        onClick={handleSell}
        className={`ml-2 w-1/2 px-4 py-2 ${
          sell ? 'border-4 py-3' : 'border'
        }  border-black ${
          buy ? 'from-gray-700' : 'bg-blue-500'
        } text-white rounded-md`}
      >
        Sell
      </button>
    </div>
  );
};
export default React.memo(ButtonBlock);
