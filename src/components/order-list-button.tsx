import React from 'react';
//This button for reserve. Switched off, instead used link
interface OrderListButtonProps {
  onShowOrderList: () => void;
}

const OrderListButton: React.FunctionComponent<OrderListButtonProps> = ({
  onShowOrderList
}) => {
  return (
    <>
      <div className="flex justify-between mt-6 mb-8">
        <button
          className=" w-full px-4 py-2 border border-black bg-blue-500 text-white rounded-md"
          onClick={onShowOrderList}
        >
          My orders
        </button>
      </div>
    </>
  );
};
export default React.memo(OrderListButton);
