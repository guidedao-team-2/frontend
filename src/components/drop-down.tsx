import { DropdownContext } from '@/contexts/dropdown-context';
import React, { useState, useContext } from 'react';

const DropDown = () => {
  const { selectedOption, setSelectedOption } = useContext(DropdownContext);
  const [isOpen, setIsOpen] = useState(false);

  const options = [
    { label: 'Выбрать валюту', value: '', image: '' },
    { label: 'BTC', value: 'BTC', image: '/bitcoin-logo.svg' },
    { label: 'ETH', value: 'ETH', image: '/ethereum-logo.svg' },
    { label: 'USDC', value: 'USDC', image: '/usdc-logo.svg' }
  ];

  const handleSelect = (option: any) => {
    setSelectedOption(option);
    setIsOpen(false);
  };

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="relative inline-block text-left">
      <div className="border border-black p-2 rounded-md dark:bg-gray-700 dark:text-gray-300">
        <div
          className="flex items-center justify-between cursor-pointer"
          onClick={toggleDropdown}
        >
          {selectedOption ? (
            <>
              {selectedOption.image && (
                // eslint-disable-next-line @next/next/no-img-element
                <img
                  src={selectedOption.image}
                  alt={selectedOption.label}
                  className="w-5 h-5 mr-2"
                />
              )}
              <span className="text-black dark:bg-gray-700 dark:text-gray-300">
                {selectedOption.label}
              </span>
            </>
          ) : (
            <span className="text-black dark:bg-gray-700 dark:text-gray-300">
              Выбрать валюту
            </span>
          )}
          <svg
            className={`w-5 h-5 ml-2 ${isOpen ? '-rotate-180' : 'rotate-0'}`}
            fill="none"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M19 9l-7 7-7-7"
            />
          </svg>
        </div>
      </div>
      {isOpen && (
        <ul className=" absolute w-full z-20 left-0 mt-2 bg-white border border-black rounded-md dark:bg-gray-700 dark:text-gray-300">
          {options.map((option) => (
            <li
              key={option.value}
              onClick={() => handleSelect(option)}
              className="px-4 py-2 cursor-pointer flex items-center"
            >
              {option.image && (
                // eslint-disable-next-line @next/next/no-img-element
                <img
                  src={option.image}
                  alt={option.label}
                  className="w-5 h-5 mr-2"
                />
              )}
              {option.label}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default React.memo(DropDown);
