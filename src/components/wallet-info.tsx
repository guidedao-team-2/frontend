/* eslint-disable prettier/prettier */
/* eslint-disable arrow-body-style */
import React, { useState, useEffect, useContext } from 'react';
import Jazzicon from 'react-jazzicon';
import { useRouter } from 'next/navigation';
import { AuthContext } from '../contexts/loging';

interface WalletInfoProps {
  onConnect: (address: string | null) => void;
}

const WalletInfo: React.FunctionComponent<WalletInfoProps> = ({
  onConnect
}) => {
  const [account, setAccount] = useState<string | null>();
  const { isLogged, setIsLogged }: any = useContext(AuthContext);
  const router = useRouter();

  useEffect(() => {
    const fetchAccount = async () => {
      try {
        if (window.ethereum) {
          const accounts = await window.ethereum.request({
            method: 'eth_requestAccounts'
          });
          const currentAccount = accounts[0];
          setAccount(currentAccount);
          onConnect(currentAccount);
          console.log(`connected: ${account}`);
        } else {
          console.error('MetaMask is not available');
        }
      } catch (error) {
        console.error('Error fetching account:', error);
      }
    };

    fetchAccount();
  }, [onConnect]);

  const handleLogout = () => {
    setAccount(null);
    setIsLogged(false);
    router.push('/Start');
  };

  const formatAddress = (address: string): string => {
    return `${address.substring(0, 5)}...${address.slice(-3)}`;
  };

  return (
    <div className="bg-gray-300 text-gray-700 dark:bg-gray-700 dark:text-gray-300 px-4 py-2 rounded flex flex-column">
      {account ? (
        <>
          <div className="flex items-center">
            <p className="mr-2">Connected: {formatAddress(account)}</p>
            <Jazzicon
              diameter={20}
              seed={parseInt(account.slice(2, 10), 16)}
            />
            <button
              className="bg-gray-300 dark:bg-gray-700 text-gray-700 dark:text-gray-300 px-4 rounded"
              onClick={handleLogout}
            >
              Logout
            </button>
          </div>
        </>
      ) : (
        <p>Not connected</p>
      )}
    </div>
  );
};

export default React.memo(WalletInfo);
