import React from 'react';
import ThemeButton from './theme-button';
import WalletInfo from './wallet-info';

const Header = () => {
  function handleConnect(address: string | null): void {
    throw new Error('Function not implemented.');
  }

  return (
    <>
      <div className="absolute left-4 mt-2">
        <ThemeButton />
      </div>
      <div className="absolute right-4 mt-2">
        <WalletInfo onConnect={handleConnect} />
      </div>
    </>
  );
};

export default React.memo(Header);
