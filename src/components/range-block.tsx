import React from 'react';

const RangeBlock = () => {
  // There will be logic of range???

  return (
    <div className="flex w-full  mt-2 mb-2 px-4 py-2 border border-blue-500 bg-black text-white rounded-md">
      <p>There will be range info</p>
    </div>
  );
};

export default React.memo(RangeBlock);
