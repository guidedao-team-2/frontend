/* eslint-disable @next/next/no-img-element */
/* eslint-disable prettier/prettier */
/* eslint-disable prefer-destructuring */
import { PurchaseContext } from '@/contexts/purchase-context';
import React, { useContext, useState } from 'react';

const InputBlock = () => {
  const { coinAmount, setCoinAmount } = useContext(PurchaseContext);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;

    // Test for positive integer
    if (/^\d+$/.test(value) && Number(value) >= 0) {
      setCoinAmount(Number(value));
      console.log(coinAmount);
    } else {
      setCoinAmount('');
    }
  };

  return (
    <div className="relative mb-2 mt-2 ">
      <input
        type="text"
        placeholder="Enter coin amount"
        value={!coinAmount ? '' : coinAmount}
        onChange={handleInputChange}
        className="border w-full bg-custom border-black p-2 rounded-l pr-10"
      />
      <div className="absolute inset-y-0 right-0 flex items-center  p-2 rounded-r">
        <img
          src="/usdc-logo.svg"
          alt="USDC"
          className="w-6 h-6 mr-2"
        />
        <span>USDC</span>
      </div>
    </div>
  );
};

export default React.memo(InputBlock);
