import {
  useState,
  createContext,
  ReactNode,
  Dispatch,
  SetStateAction
} from 'react';

interface PurchaseContextProps {
  coinAmount: number;
  setCoinAmount: Dispatch<SetStateAction<number>>;
}

export const PurchaseContext = createContext<PurchaseContextProps | any>({});

export const PurchaseProvider = ({ children }: { children: ReactNode }) => {
  const [buy, setBuy] = useState(false);
  const [sell, setSell] = useState(false);
  const [coinAmount, setCoinAmount] = useState<number>(0);
  const PurchaseContextValue = {
    buy,
    setBuy,
    sell,
    setSell,
    coinAmount,
    setCoinAmount
  };
  return (
    <PurchaseContext.Provider value={PurchaseContextValue}>
      {children}
    </PurchaseContext.Provider>
  );
};
