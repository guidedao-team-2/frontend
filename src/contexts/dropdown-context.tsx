import React, {
  useState,
  createContext,
  ReactNode,
  Dispatch,
  SetStateAction
} from 'react';

interface CurrencyOption {
  label: string;
  value: string;
  image: string;
}

interface DropdownContextProps {
  selectedOption: CurrencyOption | undefined;
  setSelectedOption: Dispatch<SetStateAction<CurrencyOption | undefined>>;
}

export const DropdownContext = createContext<DropdownContextProps>({
  selectedOption: undefined,
  setSelectedOption: () => {}
});

export const DropdownProvider = ({ children }: { children: ReactNode }) => {
  const [selectedOption, setSelectedOption] = useState<
    CurrencyOption | undefined
  >(undefined);
  const DropdownContextValue = { selectedOption, setSelectedOption };
  return (
    <DropdownContext.Provider value={DropdownContextValue}>
      {children}
    </DropdownContext.Provider>
  );
};
