import React, { useState, useEffect } from 'react';
import Link from 'next/link';

interface CurrencyOption {
  label: string;
  value: string;
  image: string;
}

const Order1 = () => {
  const [orders, setOrders] = useState<
    {
      fee: any;
      id: string;
      selectedOption: CurrencyOption | undefined;
      type: string;
      coinAmount: any;
    }[]
  >([]);

  useEffect(() => {
    const storedOrders = JSON.parse(localStorage.getItem('orders') ?? '[]');
    setOrders(storedOrders);
  }, []);

  return (
    <div>
      {orders.map((order) => (
        <div
          key={order.id}
          className="flex justify-around mt-2 mb-2 border border-blue-400 rounded p-2"
        >
          {/* Access values from the 'order' object */}
          <img
            src={order.selectedOption?.image}
            alt={order.selectedOption?.label}
            className="w-5 h-5 mr-2"
          />
          <p>{order.selectedOption?.value}</p>
          <p>{order.type}</p>
          <p>{order.fee?.currency}</p>
          <p>$ {order.fee?.value}</p>
          <p>$ {order.coinAmount}</p>
          <Link
            href={{
              pathname: '/orders/[id]',
              query: {
                id: order.id,
                coinAmount: order.coinAmount,
                value: order.fee?.value
              }
            }}
            className="bg-gray-300 dark:bg-gray-700 text-gray-700 dark:text-gray-300 px-2 py-2 rounded text-center"
          >
            <button>ORDER INFO</button>
          </Link>
        </div>
      ))}
    </div>
  );
};
export default React.memo(Order1);
