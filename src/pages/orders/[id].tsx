import { useRouter } from 'next/router';
import React from 'react';
import Link from 'next/link';
import Header from '@/components/header';

const OrderInfo1 = () => {
  const router = useRouter();
  const { id } = router.query;
  const coinAmount = router.query.coinAmount;
  const value = router.query.value;

  // Здесь вы можете использовать id для получения соответствующего заказа из локального хранилища
  // и отобразить информацию в компоненте

  return (
    <>
      <div className="box-border flex justify-center m-auto ">
        <Header />
        <div className=" flex lg:w-7/12 mt-16 border-solid border-black border bg-custom  container sm justify-center w-11/12 ">
          <div className="  flex m-6  container sm justify-center flex-col">
            <p>Order ID: {id}</p>
            {/* Остальная информация о заказе */}
            <div className="flex w-full  mt-2 mb-2 px-4 py-2 border border-blue-500 bg-gray text-white rounded-md items-center justify-between">
              <p>Order Balance $ {coinAmount} </p>
              <button className="  px-4 py-2 border border-black bg-blue-500 text-white rounded-md">
                Close Order
              </button>
            </div>

            <div className="flex w-full  mt-2 mb-2 px-4 py-2 border border-blue-500 bg-gray text-white rounded-md items-center justify-between">
              <p>Fee Balance $ {value} </p>
              <button className="  px-4 py-2 border border-black bg-blue-500 text-white rounded-md">
                Claim Fees
              </button>
            </div>

            <Link
              href={'/Order-List'}
              className="bg-gray-300 dark:bg-gray-700 text-gray-700 dark:text-gray-300 px-4 py-2 rounded text-center"
            >
              <button className="text-center">Back to Order List</button>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default OrderInfo1;
