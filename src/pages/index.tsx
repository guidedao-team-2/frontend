'use client';
import { useEffect, useContext } from 'react';
import { useRouter } from 'next/navigation';
import { AuthContext } from '@/contexts/loging';

export default function Home() {
  const { isLogged }: any = useContext(AuthContext);
  const router = useRouter();
  useEffect(() => {
    isLogged ? router.push('/Order') : router.push('/Start');
  });

  return null;
}
