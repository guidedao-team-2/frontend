/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import DropDown from '../components/drop-down';
import ButtonBlock from '../components/button-block';
import CreateOrderButton from '../components/create-order-button';
import RangeBlock from '../components/range-block';
import InputBlock from '../components/input-block';
import Link from 'next/link';
import ContractConnect from '../components/contract-connect';
import Header from '@/components/header';

interface OrderListButtonProps {
  showOrderList: boolean;
  showOrder: boolean;
}

const Order = (props: any) => {
  const handleConnect: any = '';

  return (
    <>
      <div className="box-border flex justify-center m-auto ">
        <Header />

        <div className=" flex lg:w-7/12 mt-16 border-solid border-black border bg-custom  container sm justify-center w-11/12 ">
          <div className="  flex m-6  container sm justify-center flex-col">
            <DropDown />
            <ButtonBlock />

            <InputBlock />
            <RangeBlock />
            <CreateOrderButton />
            <div className="flex justify-between mt-6 mb-8">
              <Link
                href={'/Order-List'}
                className=" w-full px-4 py-2 border border-black bg-blue-500 text-white rounded-md text-center"
              >
                <button>Show my Orders</button>
              </Link>
            </div>
          </div>
        </div>
        <div className="absolute left-10 bottom-6">
          <ContractConnect />
        </div>
      </div>
    </>
  );
};

export default Order;
