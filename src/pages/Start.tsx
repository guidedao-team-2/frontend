import Image from 'next/image';
import Background from '@/components/Background';
import { useState, useEffect } from 'react';
import WalletConnect from '@/components/wallet-connect';

export default function Start() {
  const [scale, setScale] = useState(1);

  useEffect(() => {
    const scaleInterval = setInterval(() => {
      // Increase 20%
      setScale((prevScale) => (prevScale === 1 ? 1.2 : 1));
    }, 1000); // Change the interval here to control the frequency of the animation

    return () => clearInterval(scaleInterval);
  }, []);

  return (
    <div className="relative box-border flex items-center justify-center">
      <Background />
      <Image
        style={{
          overflow: 'hidden',
          transform: `scale(${scale})`,
          transition: 'transform 1s ease'
        }}
        className="mx-auto z-10 mt-20"
        src="/logo-transp.jpg"
        width={700}
        height={700}
        alt="background"
        priority={true}
      />
      <WalletConnect />
    </div>
  );
}
