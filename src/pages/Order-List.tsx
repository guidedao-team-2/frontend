/* eslint-disable @typescript-eslint/no-unused-vars */
import Link from 'next/link';
import HeaderForOrders from '@/components/header-for-orders';
import Order1 from './orders/orders';
import Header from '@/components/header';

export default function OrderList() {
  function handleConnect(address: string | null): void {
    throw new Error('Function not implemented.');
  }

  return (
    <>
      <div className="box-border flex justify-center m-auto ">
        <Header />

        <div className=" flex lg:w-7/12 mt-16 border-solid border-black border bg-custom  container sm justify-center w-11/12 ">
          <div className="  flex m-6  container sm justify-center flex-col">
            <HeaderForOrders />
            <Order1 />

            <Link
              href={'/Order'}
              className="bg-gray-300 dark:bg-gray-700 text-gray-700 dark:text-gray-300 px-4 py-2 rounded text-center"
            >
              <button className="text-center">Back to Order</button>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}
